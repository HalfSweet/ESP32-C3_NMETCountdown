# 基于ESP32-C3的2.9寸墨水屏高考倒计时

#### 介绍
一个基于ESP32 C3的2.9寸墨水屏高考倒计时时钟


#### 安装教程

1.  首先安装Alpha版本的Arduino core for ESP32，附加开发板网址为：https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_dev_index.json
2.  修改ssid与password为你自己WiFi的ssid与密码
3.  上传SPIFFS：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/132524_9ed189d7_7831268.png "屏幕截图 2021-05-20 131807.png")

#### 使用说明

1.  引脚定义为：CS → IO2    RST → IO3    DC → IO1    BUSY → IO10    CLK → IO0    DIN → IO4，当然也可以在EPD_drive_gpio.h里面修改
2.  编译选项如图所示：![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/132707_af7d6f2a_7831268.png "屏幕截图 2021-05-20 125828.png")
3.  该驱动库由DUCK大佬编写，这是他的OSHWHub主页：[https://oshwhub.com/duck](https://oshwhub.com/duck) ，大家可以去看看
4.  原驱动库仅支持ESP8266，经过我的少量移植后支持ESP32，但是不保证完全兼容ESP8266

